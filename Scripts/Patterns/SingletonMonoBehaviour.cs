﻿namespace DRG {

    using System;
    using UnityEngine;

    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T fInstance;
        private static object lockObject = new object();
        private static bool isDestroyed = false;

        public static T instance
        {
            get
            {
                if (isDestroyed) return null;

                lock (lockObject)
                {
                    if (fInstance == null)
                    {
                        T[] instances = FindObjectsOfType<T>();
                        if (instances.Length > 1) throw new Exception("SingletonMonoBehaviour patter is broken for: " + typeof(T));
                        else if (instances.Length == 1)
                        {
                            SingletonMonoBehaviour<T> item = (instances[0] as SingletonMonoBehaviour<T>);
                            if (item.useAsInstance == false) throw new Exception("SingletonMonoBehaviour patter is broken for: " + typeof(T));
                            else return (fInstance = instances[0]);
                        }

                        fInstance = (new GameObject(typeof(T).ToString())).AddComponent<T>();
                    }
                    return fInstance;
                }
            }
        }

        [SerializeField]
        private bool useAsInstance = false;

        private void OnDestroy()
        {
            isDestroyed = true;
        }
    }

}

