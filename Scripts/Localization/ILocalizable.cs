﻿namespace DRG.Localization
{
    public interface ILocalizable
    {
        string key { get; }
        string valueLocalized { get; }
    }
}