﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace DRG.Localization
{
    [CreateAssetMenu(fileName = "Locale", menuName = "DarkRiderGames/Locale")]
    public class LocaleAsset : ScriptableObject
    {
        [SerializeField] public string[] m_Keys;
        [SerializeField] public string[] m_Values;

        public Locale GetLocale()
        {
            return new Locale(m_Keys, m_Values);
        }

        public class Locale : ILocale
        {
            private Dictionary<string, string> m_Dictionary;

            public Locale(string[] keys, string[] values)
            {
                if (keys.Length != values.Length) throw new Exception();

                m_Dictionary = new Dictionary<string, string>();
                for (int index = 0; index < keys.Length; index++)
                {
                    m_Dictionary.Add(keys[index], values[index]);
                }
            }

            public bool Has(string key)
            {
                return m_Dictionary.ContainsKey(key);
            }

            public bool TryToGet(string key, out string result)
            {
                return m_Dictionary.TryGetValue(key, out result);
            }

#if UNITY_EDITOR
            public Dictionary<string, string> dictionary
            {
                get { return m_Dictionary; }
            }
#endif
        }
    }
}