﻿using HarborCrew.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace DRG.UI
{
    [RequireComponent(typeof(Text))]
    public abstract class LocalizableObject : MonoBehaviour, ILocalizable
    {
        [Enum(
            LocaleKey.EXAMPLE_1,
            LocaleKey.EXAMPLE_2,
        )] public string m_LocaleKey;

        public string key
        {
            get { return m_LocaleKey; }
        }

        public string valueLocalized
        {
            get { return Localizer.Get(key); }
        }

        protected abstract void ApplyLocalization();

        private void Awake()
        {
            Localizer.m_EventLocaleAdded += ApplyLocalization;
        }
    }
}