﻿using UnityEngine;
using UnityEngine.UI;

namespace DRG.UI
{
    [RequireComponent(typeof(Text))]
    public class LocalizableTextField : LocalizableObject
    {
        private void Start()
        {
            ApplyLocalization();
        }

        protected override void ApplyLocalization()
        {
            GetComponent<Text>().text = valueLocalized;
        }
    }
}