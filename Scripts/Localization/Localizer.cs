﻿using System;

namespace DRG.Localization
{
    public static class Localizer
    {
        public static event Action m_EventLocaleAdded;

        private static ILocale m_Locale;

        public static string Get(string key)
        {
            string value;

            if (m_Locale != null && m_Locale.TryToGet(key, out value))
            {
                return value;
            }

#if UNITY_EDITOR
            return "#" + key;
#else
            return key;
#endif
        }

        public static void AddLocale(ILocale locale)
        {
            m_Locale = locale;
            m_EventLocaleAdded.InvokeSafe();
        }
    }
}