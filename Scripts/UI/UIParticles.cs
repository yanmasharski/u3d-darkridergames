﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(ParticleSystem))]
public class UIParticles : Graphic
{
    private ParticleSystemRenderer m_ParticleSystemRenderer;
    private ParticleSystem m_ParticleSystem;
    private ParticleSystem.Particle[] m_Particles;

    public override Material defaultMaterial
    {
        get { return (m_ParticleSystemRenderer ?? (m_ParticleSystemRenderer = GetComponent<ParticleSystemRenderer>())).sharedMaterial; }
    }

    public override Texture mainTexture
    {
        get {return defaultMaterial.mainTexture;}
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        m_ParticleSystem = m_ParticleSystem ?? GetComponent<ParticleSystem>();
        m_Particles = m_Particles ?? new ParticleSystem.Particle[m_ParticleSystem.main.maxParticles];

        var numParticlesAlive = m_ParticleSystem.GetParticles(m_Particles);

        for (var i = 0; i < numParticlesAlive; i++)
        {
            var particle = m_Particles[i];
            var size = particle.GetCurrentSize(m_ParticleSystem);
            var position = particle.position;
            var color = particle.GetCurrentColor(m_ParticleSystem);
            var rotation = particle.rotation;

            var halfSize = size * 0.5f;
            var rotationHelper = Quaternion.Euler(0, 0, -rotation);

            vh.AddVert(position + rotationHelper * new Vector3(-halfSize, -halfSize), color, new Vector2(0f, 0f));
            vh.AddVert(position + rotationHelper * new Vector3(-halfSize, halfSize), color, new Vector2(0f, 1f));
            vh.AddVert(position + rotationHelper * new Vector3(halfSize, halfSize), color, new Vector2(1f, 1f));
            vh.AddVert(position + rotationHelper * new Vector3(halfSize, -halfSize), color, new Vector2(1f, 0f));

            var delta = i * 4;

            vh.AddTriangle(delta, delta + 1, delta + 2);
            vh.AddTriangle(delta + 2, delta + 3, delta);
        }
    }

    private void Update()
    {
        m_ParticleSystem = m_ParticleSystem ?? GetComponent<ParticleSystem>();
        if (m_ParticleSystem.isPlaying) SetAllDirty();
    }
}