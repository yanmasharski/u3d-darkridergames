﻿using System.Collections.Generic;
using System.IO;
using DRG.Debug;
using DRG.Network;
using System;
using UnityEngine;
using CUETools.Codecs;
using CUETools.Codecs.FLAKE;

namespace DRG.Google.Cloud
{
    /// <summary>
    /// Google Cloud Speech API. C#
    /// Requests with Embedded Audio Content
    /// </summary>
    public class SpeechAPI
    {
        public int SAMPLE_RATE = 16000;

        private const string URL = "https://speech.googleapis.com/v1beta1/speech:syncrecognize?key={0}";
        private const string CONTENT_TYPE_KEY = "Content-Type";
        private const string CONTENT_TYPE_VALUE = "application/json";
        private const string REQUEST_FILE_NAME = "com.googleapis.speech.v1.request";
        private const string EXTENSION_FLAC = ".flac";

        private readonly ConfigGoogleCloud config;

        public SpeechAPI(ConfigGoogleCloud config)
        {
            this.config = config;
        }

        public IRequest Recognize(string wavFilePath, string lang)
        {
            // convert wav to flac
            var directoryPath = Path.GetDirectoryName(wavFilePath);
            var flacFilePath = Path.ChangeExtension(wavFilePath, EXTENSION_FLAC);
            using (var inputWavStream = new FileStream(wavFilePath, FileMode.Open))
            {
                using (var outputFlacStream = new FileStream(flacFilePath, FileMode.OpenOrCreate))
                {
                    ConvertToFlac(inputWavStream, outputFlacStream);
                }
            }

            // setup request json and save to file
            var requestData = new RequestData();
            requestData.config = new InitialRequestData()
            {
                encoding = InitialRequestData.ENCODING_FLAC,
                sampleRate = SAMPLE_RATE
            };
            requestData.audio = new AudioRequestData(flacFilePath);
            var requestDataJson = JsonUtility.ToJson(requestData);
            var requestDataPath = Path.Combine(directoryPath, REQUEST_FILE_NAME);
            File.WriteAllText(requestDataPath, requestDataJson);

            // make request
            var url = string.Format(URL, config.key);
            var fileStream = new FileStream(requestDataPath, FileMode.Open, FileAccess.Read);
            var reader = new BinaryReader(fileStream);
            var binaryData = reader.ReadBytes((int) fileStream.Length);

            reader.Close();
            fileStream.Close();

            if (null != binaryData)
            {
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);

                IRequest request = new RequestWWW(url, binaryData, headers);

                return request;
            }
            else
            {
                Log.Error("binaryData is null");
                return null;
            }
        }

        private void ConvertToFlac(Stream sourceStream, Stream destinationStream)
        {
            var audioSource = new WAVReader(null, sourceStream);
            try
            {
                if (audioSource.PCM.SampleRate != SAMPLE_RATE)
                {
                    throw new InvalidOperationException("Incorrect frequency - WAV file must be at 16 KHz.");
                }

                var buff = new AudioBuffer(audioSource, 0x10000);
                var flakeWriter = new FlakeWriter(null, destinationStream, audioSource.PCM);
                flakeWriter.CompressionLevel = 8;
                while (audioSource.Read(buff, -1) != 0)
                {
                    flakeWriter.Write(buff);
                }
                flakeWriter.Close();
            }
            finally
            {
                audioSource.Close();
            }
        }

        [Serializable]
        public class ApiResponse
        {
            public Response[] responses;

            [Serializable]
            public class Response
            {
                public Result[] results;
            }

            [Serializable]
            public class Result
            {
                public Alternative[] alternatives;
                public bool isFinal;
            }

            [Serializable]
            public class Alternative
            {
                public string transcript;
                public float confidence;
            }
        }

        [Serializable]
        private class RequestData
        {
            public InitialRequestData config;
            public AudioRequestData audio;
        }

        [Serializable]
        private class InitialRequestData
        {
            public const string ENCODING_FLAC = "FLAC";

            public string encoding;
            public int sampleRate;
        }

        [Serializable]
        private class AudioRequestData
        {
            /// <summary>
            /// BASE64
            /// </summary>
            public string content;

            public AudioRequestData(string path)
            {
                var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                var reader = new BinaryReader(fileStream);
                var binaryData = reader.ReadBytes((int) fileStream.Length);

                reader.Close();
                fileStream.Close();

                content = Convert.ToBase64String(binaryData);
            }
        }
    }
}