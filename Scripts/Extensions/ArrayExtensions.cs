﻿using UnityEngine;

public static class ArrayExtensions
{
    public static T GetRandomItem<T>(this T[] array)
    {
        if (array == null || array.Length == 0) return default(T);
        var index = Random.Range(0, array.Length);
        return array[index];
    }
}