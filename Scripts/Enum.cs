// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

using UnityEngine;

namespace DRG
{
    public class Enum : PropertyAttribute
    {
        public readonly string[] items;
        public int selected = 0;

        public Enum(System.Type type)
        {
            if (type.IsSubclassOf(typeof(Enumeration)))
            {
                var fieldInfo = type.GetField("items");
                items = (string[]) fieldInfo.GetValue(null);
            }
            else
            {
                items = new[] {"Assign Enumeration Type"};
            }
        }

        public Enum(params string[] enumerations)
        {
            items = enumerations;
        }
    }
}