// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

using UnityEditor;
using UnityEngine;

namespace DRG
{
    [CustomPropertyDrawer(typeof(Enum))]
    public class EnumDrawer : PropertyDrawer
    {
        private Enum enumeration
        {
            get { return (Enum) attribute; }
        }

        bool Start = true;

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            if (Start)
            {
                Start = false;
                for (int i = 0; i < enumeration.items.Length; i++)
                {
                    if (enumeration.items[i].Equals(prop.stringValue))
                    {
                        enumeration.selected = i;
                        break;
                    }
                }
            }

            enumeration.selected = EditorGUI.Popup(EditorGUI.PrefixLabel(position, label), enumeration.selected,
                enumeration.items);
            prop.stringValue = enumeration.items[enumeration.selected];
        }
    }
}