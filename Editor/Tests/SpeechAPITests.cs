﻿using DRG.Google.Cloud;
using NUnit.Framework;
using UnityEngine;

public class SpeechAPITests
{

    [Test]
    public void ApiResponseFromJson()
    {
        // TODO improve this test
        string jsonString = "{\"responses\": [{\"results\": [{\"alternatives\": [{\"transcript\": \"one\",\"confidence\": 1}],\"isFinal\": true}]}]}";

        var apiResponse = JsonUtility.FromJson<SpeechAPI.ApiResponse>(jsonString);

        Assert.That(apiResponse != null);

        var responses = apiResponse.responses;
        Assert.That(responses != null);
        Assert.That(responses.Length == 1);

        var response = responses[0];
        Assert.That(response != null);

        var results = response.results;
        Assert.That(results != null);
        Assert.That(results.Length == 1);

        var result = results[0];
        Assert.That(result != null);
        Assert.That(result.isFinal == true);

        var alternatives = result.alternatives;
        Assert.That(alternatives != null);
        Assert.That(alternatives.Length == 1);

        var alternative = alternatives[0];
        Assert.That(alternative != null);
        Assert.That(alternative.confidence == 1f);
        Assert.That(alternative.transcript == "one");
    }
}
